var IdSynonym, Airlines = [];

function RestFn(url, SuccessCb, ErrorCb, CancelCb) {
    $.ajax({
        type : "GET",
        dataType : "jsonp",
        url    : url,
        success : SuccessCb,
        error  : function (jqXHR, textStatus, errorThrown) {
            //throw new Error(textStatus)
        },
        cancel : function () {
            console.log("cancel")
        }
    });
}

function getIdSynonym() {
    var d = $.Deferred();
    RestFn(
        "http://api.anywayanyday.com/api/NewRequest3/?Route=2208MOWPARAD1CN0IN0SCE&_Serialize=JSON",
        function (response) {
            IdSynonym = response.IdSynonym;
            d.resolve();
        }
    );
    return d.promise();
}

function getStatus () {
    var d = $.Deferred();
    RestFn(
        "http://api.anywayanyday.com/api/RequestState/?R=" + IdSynonym + "&_Serialize=JSON",
        function (response) {
            d.resolve(parseInt(response.Completed));
        }
    );
    return d.promise();
}

function getData () {
    var d = $.Deferred();
    RestFn(
        "http://api.anywayanyday.com/api/Fares2/?R=" + IdSynonym + "&L=RU&C=RUB&Limit=200&DebugFullNames=true&_Serialize=JSON",
        function (response) {
            if (response.Error) d.reject();
            else {
                console.log(response);
                Airlines = Airlines.concat(Airlines, response.Airlines);
                $('.companies').append(renderTabHeaders(response.References.Carriers));
                d.resolve();
            }
        }
    );
    return d.promise();
}

function renderTabHeaders(Carriers) {
    var html = '';
    Carriers.map(function (line, index) {
        html += '<li data-page="'+ line.Code +'">'+ line.Name +'</li>';
        var find;
        for (var i = 0; i < Airlines.length; i++) {
            var Airline = Airlines[i];
            if (Airline.Code == line.Code) {
                find = Airline;
                break
            }
        }
        if (find && !!find.Fares.length) {
            $('.col2').append(
                '<div id="'+ find.Code + '" class="tabBody">'+
                    find.Fares.map(function (Fare) {
                        return '<h1>Цена: '+ Fare.TotalAmount +'</h1>'+
                                '<ul>' +
                                    Fare.Directions[0].Variants.map(function (Variant) {
                                        return '<li>' +
                                                '<p>Вылет - ' + moment(Variant.Legs[0].DepartureDate, 'DDMMhhmm').format('DD MM, hh:mm') +  '</p>' +
                                                '<p>Прилет - ' + moment(Variant.Legs[0].ArrivalDate, 'DDMMhhmm').format('DD MM, hh:mm') +  '</p>' +
                                            '</li>'
                                    })
                                + '</ul>'
                    })
                +'</div>'
            )
        }
    });
    return html
}


$(document).ready(function () {
    Pace.start();
    getIdSynonym().then(function () {
        var d = $.Deferred();
        var interval = setInterval(function () {
            getStatus().then(function (completed) {
                if (completed >= 100) {
                    clearInterval(interval);
                    return d.resolve();
                }
            })
        }, 2000);
        return d.promise()
    }).then(function () {
        return getData();
    }).then(function () {
        $(".tabs").lightTabs();
        Pace.stop();
    })
})

