(function($){
    jQuery.fn.lightTabs = function(options){
        var createTabs = function(){
            var tabs = this, i = 0, $tabs = $(tabs);
            var col2 = $(".col2");
            var showPage = function(i){
                col2.find(".tabBody").hide();
                col2.find("#" + i).show();
                $tabs.children("ul").children("li").removeClass("active");
                $tabs.find("[data-page =" + i + "]").addClass("active");
            };

            showPage($('[data-page]:first').data("page"))

            $(tabs).children("ul").children("li").click(function(){
                showPage($(this).data("page"));
            });
        };
        return this.each(createTabs);
    };
})(jQuery);
